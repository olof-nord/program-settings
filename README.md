# program-settings

Useful settings, plugins and configurations I find useful.

## IntelliJ IDEA

- [IntelliJ Lombok plugin](https://plugins.jetbrains.com/plugin/6317-lombok)
- [Kubernetes](https://plugins.jetbrains.com/plugin/10485-kubernetes)
- [SonarLint](https://plugins.jetbrains.com/plugin/7973-sonarlint)

## VSCode

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
- [advanced-new-file](https://marketplace.visualstudio.com/items?itemName=patbenatar.advanced-new-file)
- [File Utils](https://marketplace.visualstudio.com/items?itemName=sleistner.vscode-fileutils)
- [Gitconfig Syntax](https://marketplace.visualstudio.com/items?itemName=sidneys1.gitconfig)
- [gitignore](https://marketplace.visualstudio.com/items?itemName=codezombiech.gitignore)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [Highlight Matching Tag](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)
- [HTMLHint](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint)
- [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)

## Atom

- [Atom Beautify](https://atom.io/packages/atom-beautify)
- [Highlight selected](https://atom.io/packages/highlight-selected)
- [Minimap](https://atom.io/packages/minimap)
- [Clock](https://atom.io/packages/clock)
- [Emmet](https://atom.io/packages/emmet)

## Firefox

### Privacy & Protection

- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
- [Privacy Badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/)
- [HTTPS Everywhere](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
- [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/)
- [ClearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)
- [Redirect AMP to HTML](https://addons.mozilla.org/en-US/firefox/addon/amp2html/)
- [Privacy Possum](https://addons.mozilla.org/en-US/firefox/addon/privacy-possum/)
- [Fingerprint Shield](https://addons.mozilla.org/en-US/firefox/addon/fingerprint-shield/)

### Web development

- [ColorZilla](https://addons.mozilla.org/en-US/firefox/addon/colorzilla/)
- [Font Finder (revived)](https://addons.mozilla.org/en-US/firefox/addon/font-inspect/)
- [Redux DevTools](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
- [Resize Window & Viewport](https://addons.mozilla.org/en-US/firefox/addon/resize-window-viewport/)

## Printer driver

This works for Canon Pixma TS3350 scan/print
- [canon-pixma-ts5055-complete](https://aur.archlinux.org/packages/canon-pixma-ts5055-complete/)

This is a good starting point for any Canon Windows drivers
- [ij.start.canon](https://ij.start.canon/)

- CUPS administration web GUI: [localhost:631/admin](http://localhost:631/admin)

## OpenWRT

- [Advanced Reboot Web UI](https://docs.openwrt.melmac.net/luci-app-advanced-reboot/)
- [p910nd Print Server](https://openwrt.org/docs/guide-user/services/print_server/p910ndprinterserver)
- [saned Scanner Server](https://openwrt.org/docs/guide-user/services/scanner_server/saned)
- [sane-pixma](https://openwrt.org/packages/pkgdata/sane-pixma)
- [sane-canon](https://openwrt.org/packages/pkgdata/sane-canon)

## GNOME Shell

- [Vitals](https://extensions.gnome.org/extension/1460/vitals/)

Repository icon made by [Freepik](https://www.flaticon.com/authors/freepik).
